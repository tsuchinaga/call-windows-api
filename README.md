# call windows api

Go言語のsys/windowsを使ってWindows APIを叩く。


## 環境
* Windows 10 64bit
* Go 1.15


## やったこと

* 起動からの時間を1秒ごとに表示する
    * [tick_count](tick_count)
    * 引数なしで呼べて、戻り値を受け取れる例
* メッセージを表示する
    * [message_box](message_box)
    * 引数が少なく、GUIが表示される例
* ウィンドウを生成する
    * [create_window](create_window)
    * ウィンドウが実際に生成されなかった
    * 引数の値に問題があると思うけど、理解するにはもっとWindowsAPIを理解しないといけなさそうってことでいったん中断
