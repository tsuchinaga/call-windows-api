package main

import (
	"log"
	"time"

	"golang.org/x/sys/windows"
)

func init() {
	log.SetFlags(log.Llongfile | log.LstdFlags)
}

func main() {
	getTickCount := windows.NewLazyDLL("kernel32.dll").NewProc("GetTickCount")

	for {
		<-time.After(time.Second)
		r1, r2, err := getTickCount.Call()
		log.Println(r1, r2, err)
	}
}
