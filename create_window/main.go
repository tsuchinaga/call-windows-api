package main

import (
	"log"
	"unsafe"

	"golang.org/x/sys/windows"
)

func init() {
	log.SetFlags(log.Llongfile | log.LstdFlags)
}

func main() {
	createWindow := windows.NewLazyDLL("user32.dll").NewProc("CreateWindowExW")
	if createWindow.Find() != nil {
		log.Fatalln(createWindow.Find())
	}

	var dwExStyle ExStyle = WS_EX_LEFT
	var lpClassName LPCTSTR = "STATIC"
	var lpWindowName LPCTSTR = "うぃんどうの名前"
	var dwStyle DWORD = 0
	var x INT = 100
	var y INT = 100
	var nWidth INT = 200
	var nHeight INT = 200
	var hWndParent HWND = 0
	var hMenu HMENU = 0
	var hInstance HINSTANCE = 0
	var lpParam LPVOID = 0

	ret1, ret2, err := createWindow.Call(
		dwExStyle.uintptr(),
		lpClassName.uintptr(),
		lpWindowName.uintptr(),
		dwStyle.uintptr(),
		x.uintptr(),
		y.uintptr(),
		nWidth.uintptr(),
		nHeight.uintptr(),
		hWndParent.uintptr(),
		hMenu.uintptr(),
		hInstance.uintptr(),
		lpParam.uintptr())
	log.Println(ret1, ret2, err)
}

type HANDLE uintptr
type HWND HANDLE

func (t HWND) uintptr() uintptr {
	return uintptr(t)
}

type HMENU HANDLE

func (t HMENU) uintptr() uintptr {
	return uintptr(t)
}

type HINSTANCE HANDLE

func (t HINSTANCE) uintptr() uintptr {
	return uintptr(t)
}

type LPCTSTR string

func (t LPCTSTR) uintptr() uintptr {
	return uintptr(unsafe.Pointer(windows.StringToUTF16Ptr(string(t))))
}

type ULONG uint32

type DWORD ULONG

func (t DWORD) uintptr() uintptr {
	return uintptr(t)
}

type INT int32

func (t INT) uintptr() uintptr {
	return uintptr(t)
}

type LPVOID uintptr

func (t LPVOID) uintptr() uintptr {
	return uintptr(t)
}

type ExStyle DWORD

func (t ExStyle) uintptr() uintptr {
	return uintptr(t)
}

const (
	WS_EX_LEFT ExStyle = 0x00000000
	// 以下略
)
