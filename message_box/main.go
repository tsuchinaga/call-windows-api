package main

import (
	"log"
	"unsafe"

	"golang.org/x/sys/windows"
)

func init() {
	log.SetFlags(log.LstdFlags | log.Llongfile)
}

func main() {
	messageBox := windows.NewLazyDLL("user32.dll").NewProc("MessageBoxW")
	if messageBox.Find() != nil {
		log.Fatalln(messageBox.Find())
	}

	var hWnd HWND = 0
	var lpText LPCTSTR = "メッセージテキスト\n改行も入れてみる"
	var lpCaption LPCTSTR = "メッセージタイトル"
	var uType MBType = MBTypeOK

	messageBox.Call(
		hWnd.uintptr(),
		lpText.uintptr(),
		lpCaption.uintptr(),
		uType.uintptr())
}

type HWND uintptr

func (t HWND) uintptr() uintptr {
	return uintptr(t)
}

type LPCTSTR string

func (t LPCTSTR) uintptr() uintptr {
	return uintptr(unsafe.Pointer(windows.StringToUTF16Ptr(string(t))))
}

type UINT uint32
type MBType UINT

func (t MBType) uintptr() uintptr {
	return uintptr(t)
}

const (
	MBTypeOK MBType = 0x00000000
	// 以下略
)
